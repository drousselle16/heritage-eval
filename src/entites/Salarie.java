
package entites;

public class Salarie {
    
    private String matricule;
    private String nomprenom;
    private Float  salaireDeBase;
    private String sexe;
    
    private SecteurGeo leSecteurGeo;
    private Float salaireBrut;
    
    public void afficher(){
        System.out.println(matricule+" "+nomprenom+" "+salaireBrut);
    }
    public Float salaireBrut(){
        salaireBrut=salaireDeBase;
    return salaireDeBase;
    }

   
    //<editor-fold defaultstate="collapsed" desc="gets & sets">
    
    public String getMatricule() {
        return matricule;
    }
    
    public String getNomprenom() {
        return nomprenom;
    }
    
    public Float getSalaireDeBase() {
        return salaireDeBase;
    }
    
    public String getSexe() {
        return sexe;
    }
    
    public SecteurGeo getLeSecteurGeo() {
        return leSecteurGeo;
    }
    
    public void setMatricule(String matricule) {
        this.matricule = matricule;
    }
    
    public void setNomprenom(String nomprenom) {
        this.nomprenom = nomprenom;
    }
    
    public void setSalaireDeBase(Float salaireDeBase) {
        this.salaireDeBase = salaireDeBase;
    }
    
    public void setSexe(String sexe) {
        this.sexe = sexe;
    }
    
    public void setLeSecteurGeo(SecteurGeo leSecteurGeo) {
        this.leSecteurGeo = leSecteurGeo;
    }
    //</editor-fold>
}

